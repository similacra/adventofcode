from os import linesep

def parse_input(input: str):
    points= []
    folds = []
    lines = [x.strip() for x in input.split(linesep) if len(x) > 0]
    for line in lines:
        if line.find(",") > 0:
            x, y = [x for x in map(int, line.split(','))]
            points.append((x, y))
        if line.find("=") > 0:
            tmp = line[len("fold along "):].split("=")
            ax, val = tmp
            folds.append((ax, int(val)))

    xmx = max(map(lambda c: c[0], points)) + 1
    ymx = max(map(lambda c: c[1], points)) + 1
    grid = []
    for j in range(ymx):
        grid.append([])
        for i in range(xmx):
            if (i, j) in points:
                grid[j].append("#")
                continue
            grid[j].append(".")
    return grid, points, folds

def copy_grid(grid: list, x=None, y=None):
    result = []
    flipped = []
    if x:
        for j, row in enumerate(grid):
            result.append([])
            flipped.append([])
            for i, val in enumerate(row):
                if i < x:
                    result[j].append(val)
                if i >= x:
                    flipped[j].append(val)
        tmp = []
        for l in flipped:
            rw = []
            for c in reversed(l):
                rw.append(c)
            tmp.append(rw)

        flipped = tmp
    if y:
        for j in range(y):
            result.append([])
            for i, val in enumerate(grid[j]):
                result[j].append(val)
        for j in range(len(grid) - y):
            flipped.append([])
            for i,val in enumerate(grid[j + y]):
                flipped[j].append(val)
        flipped.reverse()


    return result, flipped

def print_grid(grid: list, mapper=str):
    for _, row in enumerate(grid):
        print("".join(map(mapper, row)))

def merge_grids(grid, flipp):
    result = []
    for j, row in enumerate(grid):
        result.append([])
        for i, val in enumerate(row):
            fval = flipp[j][i]
            if fval == "." and val == ".":
                result[j].append(".")
            else:
                result[j].append("#")
    return result

def calculate_dots_after_folds(input: str, op_count= 1):
    grid, _, folds = parse_input(input)

    itr = 0
    for axis, val in folds:
        if axis == 'y':
            nxt, flipped = copy_grid(grid, y=val)
            mrgd = merge_grids(nxt, flipped)
        if axis == 'x':
            nxt, flipped = copy_grid(grid, x=val)
            mrgd = merge_grids(nxt, flipped)
        grid = mrgd
        itr += 1
        if itr == op_count:
            break

    result = 0
    for j, row in enumerate(mrgd):
        for i, val in enumerate(row):
            if val == "#":
                result += 1
    return result


#Rework
def buildgrid(width = 10, height = 10):
    grid = []
    for j in range(height):
        grid.append([])
        for i in range(width):
            grid[j].append(0)
    return grid

def points_to_grid(points: list):
    xmx = max(map(lambda c: c[0], points))
    ymx = max(map(lambda c: c[1], points))
    grid = buildgrid(width=xmx + 1, height=ymx + 1)

    for x, y in points:
        grid[y][x] = 1
    return grid

def fold(grid:list, x=None, y=None, folder= lambda a, b: max(a, b)):
    lcl = grid.copy()
    if y:
        for j, row in enumerate(lcl[y:]):
            for i, val in enumerate(row):
                old = lcl[y-j][i]
                lcl[y - j][i] = folder(old, val)
        return lcl[:y]
    if x:
        for j, row in enumerate(lcl):
            for i, val in enumerate(row[x:]):
                old = lcl[j][x - i]
                lcl[j][x - i] = folder(old, val)
        return [t[0:x] for t in lcl]

def one_count(main_grid: list):
    cnt = 0
    for _, row in enumerate(main_grid):
        cnt += sum(row)

    return cnt

def process_folds(data: str, fold_count=999999):
    _, points, folds = parse_input(data)
    main_grid = points_to_grid(points)

    fold_itr = 0
    for f, l in folds:
        if f == 'y':
            main_grid = fold(main_grid, y=l)
        if f == 'x':
            main_grid = fold(main_grid, x=l)
        fold_itr +=1
        if fold_itr >= fold_count:
            break
    return main_grid
