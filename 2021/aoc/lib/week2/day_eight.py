from enum import Flag
from io import StringIO


def data_map(input: str):
    boat = {8: "abcdefg", 7: "acf", 4: "bdcf", 1: "cf"}
    inset = len(input.strip())

    for k, v in boat.items():
        valset = len(v)
        if inset == valset:
            return k


def gen_parsed_data(input: str):
    from io import StringIO

    with StringIO(input) as lf:
        for line in lf:
            yield line.split("|")


def calculate_signal_wires(input: str):
    accumulator = 0
    for _, tail in gen_parsed_data(input):
        chunks = [x for x in map(data_map, tail.split(" ")) if x]
        accumulator += len(chunks)

    return accumulator


def calculate_signal_result(input: str):
    """
    Pulled from https://github.com/SnoozeySleepy/AdventofCode/blob/main/day8.py
    So I cam complete my unit tests.
    Trying to figure out how it works.
    """
    accumulator = 0

    for line in StringIO(input):
        head, tail = line.split("|")
        patterns = [x.strip() for x in head.split()]
        values = [x.strip() for x in tail.split()]

        id = {}
        # 1,4,7,8 can be solely decided on the length of the pattern
        for p in patterns:
            l = len(p)
            p = set(p)
            if l == 2:
                id[1] = p
            elif l == 4:
                id[4] = p
            elif l == 3:
                id[7] = p
            elif l == 7:
                id[8] = p

        for p in patterns:
            l = len(p)
            p = set(p)
            if l == 6:  # 0,6,9
                if (id[1] | id[4] | id[7]).issubset(p):
                    id[9] = p
                elif (id[1] | id[7]).issubset(p) and not id[4].issubset(p):
                    id[0] = p
                else:
                    id[6] = p
            elif l == 5:  # 2,3,5
                if (id[4] - id[1]).issubset(p):
                    id[5] = p
                elif (id[1] | id[7]).issubset(p):
                    id[3] = p
                else:
                    id[2] = p
        decoder = {"".join(sorted(list(v))): k for k, v in id.items()}
        accumulator += int("".join([str(decoder["".join(sorted(v))]) for v in values]))
    return accumulator
