from io import StringIO

charset = {"<": ">", "[": "]", "(": ")", "{": "}"}
lefts, rights = ([], [])
for k, v in charset.items():
    lefts.append(k)
    rights.append(v)


def build_stack(line: str):
    q = []
    for i in range(len(line)):
        c = line[i]
        if len(q) < 1:
            q.append(c)
            continue
        if c in lefts:
            q.append(c)
        elif c in rights:
            last = q[-1]
            if charset[last] == c:
                q.pop()
                continue
            else:
                return (c, line[i + 1], q)
    return (None, None, q)


numerics = {")": 3, "]": 57, "}": 1197, ">": 25137}


def find_corrupted_lines(input: str):
    lines = [x.strip() for x in StringIO(input)]

    processed = [build_stack(x) for x in lines if len(x.strip()) > 0]
    corrupts = [x for x, _, _ in processed if x]
    val = 0
    for b in corrupts:
        val += numerics[b]
    return val


def autocomplete_line(line: list):
    result = []
    while len(line) > 0:
        current = line.pop()
        for k, v in charset.items():
            if current == k:
                result.append(v)
    return "".join(result)


def score_completion(line: str):
    scores = {")": 1, "]": 2, "}": 3, ">": 4}
    score = 0
    for c in line:
        score *= 5
        score += scores[c]
    return score


def repair_corrupted_lines(input: str):
    lines = [x.strip() for x in StringIO(input)]

    processed = [build_stack(x) for x in lines if len(x.strip()) > 0]
    corrupts = [z for x, _, z in processed if x is None]

    scores = []
    for a in corrupts:
        finished = autocomplete_line(a)
        scores.append(score_completion(finished))
    ordered = sorted(scores)
    ndx = int((len(ordered) - 1) / 2)
    return ordered[ndx]
