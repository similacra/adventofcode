from os import linesep


def process(grid: list):
    flashed = []

    def increment_all():
        for i, row in enumerate(grid):
            for j, val in enumerate(row):
                grid[i][j] = val + 1

    def getflashnodes():
        for i, row in enumerate(grid):
            for j, val in enumerate(row):
                if (i, j) in flashed:
                    continue
                if val > 9:
                    yield (i, j)

    def point_in_grid(i, j):
        if i < 0 or i >= len(grid):
            return
        if j < 0 or j >= len(grid[i]):
            return
        return True

    def get_neighbors(i, j):
        neighbors = [
            (x + i, y + j)
            for x in range(-1, 2)
            for y in range(-1, 2)
            if x != 0 or y != 0
        ]
        valid = [(x, y) for x, y in neighbors if point_in_grid(x, y)]
        return valid

    def flashnodes():
        flashes = 0
        toflash = list(getflashnodes())
        while toflash:
            for (x, y) in toflash:
                flashed.append((x, y))
                grid[x][y] = 0
                for r, s in get_neighbors(x, y):
                    if (r, s) not in flashed:
                        grid[r][s] += 1
            toflash = list(getflashnodes())

    increment_all()
    flashnodes()
    return (grid, len(flashed))


def all_flashed(grid: list):
    result = True
    for _, row in enumerate(grid):
        result &= all([x == 0 for x in row])
    return result


def calculate_flashing_octopus(input: str, days: int):
    tot_flashes = 0
    grid = []
    for line in [x.strip() for x in input.split(linesep)]:
        grid.append([int(x) for x in line])
    for x in range(days):
        grid, cnt = process(grid)
        tot_flashes += cnt

    return tot_flashes


def calculate_synced_flash(input: str):
    grid = []
    for line in [x.strip() for x in input.split(linesep)]:
        grid.append([int(x) for x in line])

    num = 0
    while not all_flashed(grid):
        grid, _ = process(grid)
        num += 1
    return num
