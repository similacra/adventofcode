from os import linesep
from collections import deque

def isVisited(item: str, path: list, smallTwice = False):
    if item.isupper():
        return False

    return item in path

def find_path(graph: list[list[int]], smallTwice = False, src = 'start', dst = 'end'):
    q = deque()

    paths = []
    path = []
    path.append(src)
    q.append(path.copy())

    while q:
        path = q.popleft()
        last = path[-1]

        if (last == dst):
            paths.append(path)

        for i, val in enumerate(graph[last]):
            if not isVisited(val, path, smallTwice):
                newpath = path.copy()
                newpath.append(val)
                q.append(newpath)
    return paths

def parse_grid(input:str):
    lines = [x for x in input.split(linesep)]

    graph = {}
    for a, b in [x.split("-") for x in lines]:
        if a not in graph.keys():
            graph[a] = [b]
        else:
            if b not in graph[a]:
                graph[a].append(b)
        if b not in graph.keys():
            graph[b] = [a]
        else:
            if a not in graph[b]:
                graph[b].append(a)
    return graph

def compute_paths(input: str, smallTwice=False):
    graph = parse_grid(input)

    paths = find_path(graph, smallTwice)
    return len(paths)

def better_compute_paths(input: str, smallTwice=False):
    grid = parse_grid(input)

    def count_paths(node="start", visited={"start"}, smlTwice=False):
        if node == "end":
            return 1
        count = 0
        for nb in grid[node]:
            if nb.isupper():
                count += count_paths(nb, visited, smlTwice)
            elif nb not in visited:
                count += count_paths(nb, visited | {nb}, smlTwice)
            elif smlTwice and nb != "start":
                count += count_paths(nb, visited, False)
        return count

    return count_paths(smlTwice=smallTwice)
