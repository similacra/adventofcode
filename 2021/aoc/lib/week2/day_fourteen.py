from collections import Counter, defaultdict
from os import device_encoding, linesep, read
from typing import DefaultDict

def read_input(input: str):
    lines = [x.strip() for x in input.split(linesep)]

    template = None
    rules = []
    for line in [x for x in lines if len(x) > 0]:
        if template is None and line.find("->") < 0:
            template = line
            continue
        pair, insert = [x.strip() for x in line.split("->")]
        rules.append((pair, insert))
    return template, rules

def perform_inefficient_step(template: str, rules: list):
    current = []
    for pair, item in rules:
        inc = 0
        ndx = template.find(pair)
        while ndx >= 0:
            current.append((pair, item, ndx + 1))
            inc = ndx + 1
            ndx = template.find(pair, inc)

    pts = list(template)
    for pair, item, ndx in sorted(current, key= lambda c: c[2], reverse=True):
        pts.insert(ndx, item)

    return "".join(pts)

def compute_polymerization(input: str, steps=10):
    template, rules = read_input(input)

    current = template
    for _ in range(steps):
        current = perform_inefficient_step(current, rules)

    cntr = defaultdict(int)
    for c in current:
        cntr[c] += 1

    vals = sorted(cntr.values())
    return vals[-1] - vals[0]


def compute_better_polymerization(input: str, steps=10):
    template, rules_list = read_input(input)
    rules = {}
    for k, v in rules_list:
        rules[k] = v

    def better_step(last: dict, letters: dict):
        result = defaultdict(int)
        for pair, val in last.items():
            letter = rules[pair]
            a, b = list(pair)
            result[a + letter] += last[pair]
            result[letter + b] += val
            letters[letter] += last[pair]

        return result, letters

    char_count = Counter(template)
    pairs = Counter([f"{val}{template[i+1]}" for i,
                    val in enumerate(template) if i < len(template) - 1])

    for _ in range(steps):
        pairs, char_count = better_step(pairs, char_count)

    vals = sorted(char_count.values())
    return vals[-1] - vals[0]
