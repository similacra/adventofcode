from io import StringIO


def get_lowpoints(grid: list, give_points=False):
    for i, row in enumerate(grid):
        for j, val in enumerate(row):
            left, right, top, bottom = (9, 9, 9, 9)
            if j > 0:
                left = grid[i][j - 1]
            if j < len(grid[i]) - 1:
                right = grid[i][j + 1]
            if i > 0:
                top = grid[i - 1][j]
            if i < len(grid) - 1:
                bottom = grid[i + 1][j]
            if val < min([left, right, bottom, top]):
                if give_points:
                    yield (i, j)
                else:
                    yield val


def calculate_risk_level(input: str):
    grid = []
    for line in StringIO(input):
        grid.append([int(x) for x in line.strip()])

    return sum(map(lambda c: c + 1, get_lowpoints(grid)))


def get_location_value(grid: list, i: int, j: int):
    if i < 0 or i >= len(grid):
        return 9
    if j < 0 or j >= len(grid[i]):
        return 9

    return grid[i][j]


def find_basin_points(grid: list, i: int, j: int):
    points = []

    def evaluate(i: int, j: int):
        if (i, j) in points:
            return get_location_value(grid, i, j)
        iterate_points(i, j)

    def iterate_points(i: int, j: int):
        if (i, j) in points:
            return None
        points.append((i, j))
        neighbors = [(i, j - 1), (i, j + 1), (i - 1, j), (i + 1, j)]
        values = [(i, j) for i, j in neighbors if get_location_value(grid, i, j) < 9]
        for x in values:
            evaluate(*x)

    iterate_points(i, j)
    return points


def determine_basin_size(grid: list, i: int, j: int):
    points = find_basin_points(grid, i, j)
    return len(points)


def find_basins(input: str):
    grid = []
    for line in StringIO(input):
        grid.append([int(x) for x in line.strip()])

    vals = []
    for x, y in get_lowpoints(grid, True):
        vals.append(determine_basin_size(grid, x, y))
    ordered = sorted(vals, reverse=True)
    result = 1
    for i in range(3):
        result *= ordered[i]
    return result


def grid_with_rich(input: str, tofile=None):
    from rich.color import Color
    from rich.console import Console

    grid = []
    colours = []
    for line in StringIO(input):
        grid.append([int(x) for x in line.strip()])
        colours.append([f"[red][b]9[/b]" for x in line.strip()])

    colors = ["yellow", "hot_pink", "green1", "blue_violet", "green3", "cyan1"]

    basins = []

    for x, y in get_lowpoints(grid, True):
        basins.append((x, y, find_basin_points(grid, x, y)))

    for x, y, points in basins:
        colour = colors.pop()
        colors.insert(0, colour)
        for i, j in points:
            val = grid[i][j]
            colours[i][j] = f"[{colour}]{val}"

    cons = Console()
    for row in colours:
        cons.print(" ".join(row))
