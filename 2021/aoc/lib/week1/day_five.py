from aoc.lib.dto import Direction, Line
from aoc.lib.funcs import collinear


def parse_lines(input: str):
    def parse_point(data: str):
        x, y = list(map(float, [x.strip() for x in data.split(",")]))
        return (x, y)

    from io import StringIO

    with StringIO(input) as fl:
        for line in fl:
            start, end = [x.strip() for x in line.strip().split("->")]
            yield (parse_point(start), parse_point(end))


def board(grid_size: int = 10):
    for i in range(grid_size):
        for j in range(grid_size):
            yield (float(i), float(j))


def point_in_range(start, end, x, y):
    x1, y1 = start
    x2, y2 = end

    x_inrange = min(x1, x2) <= x and x <= max(x1, x2)
    y_inrange = min(y1, y2) <= y and y <= max(y1, y2)

    return x_inrange and y_inrange and collinear(x1, y1, x2, y2, x, y)


def build_grid(input: str, include_diagonal=True):
    def group_by(items: list, keyfunc=lambda c: c):
        boat = {}
        for item in items:
            key = keyfunc(item)
            if key not in boat.keys():
                boat[key] = []
            boat[key].append(item)
        return boat

    lines = []
    for start, end in parse_lines(input):
        ln = Line(start, end)
        if not include_diagonal and ln.direction == Direction.DIAGONAL:
            continue
        lines.append(ln)
    bucket = []
    for line in lines:
        bucket.extend([line.start, line.end])
        bucket.extend(line.parts)

    groups = group_by(bucket)

    vals = [k for k, v in groups.items() if len(v) >= 2]
    return len(vals)
