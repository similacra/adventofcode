from io import StringIO
from aoc.lib.dto import Board


def parse_boards(input: str):
    calls = None
    boards = []
    with StringIO(input) as fl:
        calls = [int(x) for x in fl.readline().strip().split(",")]
        row_num = 0
        current = None

        for line in fl:
            if len(line.strip()) < 1:
                continue
            row = line.strip()
            vals = list(map(int, [x for x in row.split(" ") if len(x) > 0]))

            if row_num == 0:
                if current:
                    boards.append(current)
                current = Board()

            for i, val in enumerate(vals):
                current.rows[row_num][i] = val
            row_num = (row_num + 1) % 5
        if current not in boards:
            boards.append(current)
    return (calls, boards)


def find_winner(board: Board, items: list):
    for i, val in enumerate(items):
        board.check_value(val)

        if board.bingo:
            return (i, val, board)


def solve(input: str, last=False):
    calls, boards = parse_boards(input)

    def mapper(b):
        return find_winner(b, calls)

    solved = list(map(mapper, boards))

    ordered = sorted(solved, key=lambda c: c[0], reverse=last)
    if len(ordered) > 0:
        _, winner, board = ordered[0]
        return winner * board.unmarked
