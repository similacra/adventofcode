from io import StringIO


class LanternFish:
    def __init__(self, timer_start: int, add_fish: callable):
        self.__timer = timer_start
        self.__day = 0
        self.__add_fish = add_fish

    @property
    def timer(self):
        return self.__timer

    @property
    def day(self):
        return self.__day

    @property
    def add_fish(self):
        return self.__add_fish

    def __str__(self):
        return f"Timer: {self.timer} Day: {self.day}"

    def advance(self):
        self.__day += 1
        self.__timer -= 1

        if self.__timer < 0:
            self.__timer = 6
            self.add_fish()


def process(values: dict):
    result = {8: 0}
    toadd = 0
    for k, v in values.items():
        key = k - 1

        if key < 0:
            toadd = v
        else:
            result[key] = v

    result[6] += toadd
    result[8] += toadd

    return result


def calculate(input: str, days: int):
    vals = [int(x.strip()) for x in input.split(",")]
    holds = {}
    for i in range(9):
        holds[i] = 0
    for val in vals:
        holds[val] += 1

    for i in range(1, days + 1):
        holds = process(holds)

    tot = 0
    for k, v in holds.items():
        tot += v
    return tot
