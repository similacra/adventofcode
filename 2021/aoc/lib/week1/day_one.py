def calc_day_1_1(items: list):
    val = 0

    for i in range(len(items)):
        if i < 1:
            continue
        if items[i] > items[i - 1]:
            val += 1
    return val


def calc_day_1_2(items: list, prev=0, summer=0):
    if len(items) < 3:
        return summer - 1

    a, b, c = items[0:3]
    current = a + b + c
    summer += 1 if (current > prev) else 0
    return calc_day_1_2(items[1:], current, summer)


class WindowedIncreaser:
    def __init__(self, window_size=1) -> None:
        self.__window_size = window_size

    @property
    def window_size(self):
        return self.__window_size

    def __call__(self, items: list):
        def reduce(input: list):
            whole = [input[i : i + self.window_size] for i in range(len(input))]
            filtered = [x for x in whole if len(x) >= self.window_size]
            all = [sum(x) for x in filtered]
            return all

        def calculate(items: list):
            val = 0
            for i in range(1, len(items)):
                if items[i] > items[i - 1]:
                    val += 1
            return val

        return calculate(reduce(items))
