import math


def count_vals(items: list):
    vals = {}
    for x in items:
        if x in vals.keys():
            vals[x] += 1
        else:
            vals[x] = 1
    return vals


def more_or_less(items, more=True):
    counter = count_vals(items)
    ordered = sorted(counter.items(), key=lambda c: c[1], reverse=more)
    if len(ordered) >= 1:
        k, _ = ordered[0]
        return k


def compute_gamma(items: list):
    val = len(items[0])

    bins = []
    for i in range(val):
        columns = [x[i] for x in items]
        bins.append(more_or_less(columns))

    val = "".join(bins)
    return int(val, 2)


def compute_epsilom(items: list):
    val = len(items[0])

    bins = []
    for i in range(val):
        columns = [x[i] for x in items]
        bins.append(more_or_less(columns, False))

    val = "".join(bins)
    return int(val, 2)


def compute_O2_rating(items: list):
    def inner(items: list, index: int, top: int):
        if index >= top:
            return items
        if len(items) == 1:
            return items
        columns = [x[index] for x in items]
        vals = count_vals(columns)
        zeros_val = vals["0"]
        ones_val = vals["1"]

        most = "0" if zeros_val > ones_val else "1"

        if zeros_val == ones_val:
            most = "1"

        return inner([x for x in items if x[index] == most], index + 1, top)

    value, *_ = inner(items, 0, len(items[0]))
    return int(value, 2)


def compute_CO2_rating(items: list):
    def inner(items: list, index: int, top: int):
        if index >= top:
            return items
        elif len(items) == 1:
            return items
        columns = [x[index] for x in items]
        vals = count_vals(columns)
        zeros_val = vals["0"]
        ones_val = vals["1"]

        most = "0" if zeros_val < ones_val else "1"

        if zeros_val == ones_val:
            most = "0"

        return inner([x for x in items if x[index] == most], index + 1, top)

    value, *_ = inner(items, 0, len(items[0]))
    return int(value, 2)
