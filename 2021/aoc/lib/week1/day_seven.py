def calulate_line_up_point(crabs: list, meet: int, accumulate: bool):
    def calculate(current: int):
        import math

        n = math.sqrt(math.pow(current - meet, 2))
        if accumulate:
            return 0.5 * (n * (n + 1))
        return n

    return list(map(calculate, crabs))


def calculate_fuel_cost(input: str, accumulate=False):
    crabs = [int(x) for x in input.split(",")]
    maxim = max(crabs)
    minim = min(crabs)

    results = []
    for i in range(minim, maxim):
        val = sum(calulate_line_up_point(crabs, i, accumulate))
        results.append((i, val))

    ordered = sorted(results, key=lambda c: c[1])
    if len(ordered) > 0:
        i, val = ordered[0]
        return int(val)

    return -1
