from enum import Enum


class Direction(Enum):
    Up = 1
    Down = 2
    Forward = 3


class Move:
    def __init__(self, direction, amount: int):
        self.__direction = direction
        self.__amount = amount

    @property
    def direction(self):
        return self.__direction

    @property
    def amount(self):
        return self.__amount


def perform_moves(move_list: list):
    x, y = (0, 0)

    for move in move_list:
        if move.direction == Direction.Up:
            y -= move.amount
        elif move.direction == Direction.Down:
            y += move.amount
        elif move.direction == Direction.Forward:
            x += move.amount

    return x, y


def perform_moves_with_aim(move_list: list):
    x, y, aim = (0, 0, 0)
    for move in move_list:
        if move.direction == Direction.Up:
            aim -= move.amount
        elif move.direction == Direction.Down:
            aim += move.amount
        elif move.direction == Direction.Forward:
            x += move.amount
            y += move.amount * aim

    return x, y
