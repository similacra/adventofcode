
class Die:
    def __init__(self) -> None:
        self.__ndx = 0
        self.__rollcount = 0
        self.__die = list([x + 1 for x in range(100)])
    @property
    def ndx(self):
        return self.__ndx
    @ndx.setter
    def ndx(self, val):
        self.__ndx = val
    @property
    def rollcount(self):
        return self.__rollcount
    @rollcount.setter
    def rollcount(self, val):
        self.__rollcount = val
    def roll(self):
        self.rollcount += 1
        val = self.__die[self.ndx]
        self.ndx = (self.ndx + 1) % len(self.__die)
        return val

board = [ 1, 2, 3, 4, 5, 6, 7 ,8 ,9, 10]

class Player:
    def __init__(self, position: int, die: Die):
        self.__position = position
        self.__score = 0
        self.__die = die

    @property
    def score(self):
        return self.__score

    def roll(self):
        return self.__die.roll()

    def playturn(self):
        global board
        val = 0
        for _ in range(3):
            val += self.roll()
        npos = (self.__position + val) % len(board)
        self.__position = npos
        self.__score += board[npos]

def calculate(start_one=4, start_two=8, scoregoal=1000):
    die = Die()
    p_one = Player(start_one - 1, die)
    p_two = Player(start_two - 1, die)

    while p_one.score < scoregoal and p_two.score < scoregoal:
        p_one.playturn()
        if p_one.score >= scoregoal:
            break

        p_two.playturn()
        if p_two.score >= scoregoal:
            break

    return min(p_one.score, p_two.score) * die.rollcount
