


def perform_step(vx, vy, x, y):
    x += vx
    y += vy

    if vx > 0:
        vx -= 1
    elif vx < 0:
        vx += 1
    vy -= 1
    return (vx, vy, x, y)

def parse_input(data: str):
    #target area: x=230..283, y=-107..-57
    import re
    xblock = re.search(r"x=([\d\-]+)[^\d\-]*([\d\-]+)", data)
    yblock = re.search(r"y=([\d\-]+)[^\d\-]*([\d\-]+)", data)

    xrange = list(map(int, xblock.groups()))
    yrange = list(map(int, yblock.groups()))
    return xrange, yrange


def build_target_area(xs: list[int], ys: list[int]):
    from itertools import product
    xmin, xmax = (min(xs), max(xs))
    ymin, ymax = (min(ys), max(ys))

    result = []
    for c in product(range(xmin, xmax + 1), range(ymin, ymax + 1)):
        result.append(c)
    return sorted(result, key= lambda c: c[::-1])

def calculate_distance(point: tuple, start=(0, 0)):
    from math import pow, sqrt
    (x, y) = point
    (sx, sy) = start

    if x == sx:
        return abs(y - sy)
    return  sqrt(pow(x - sx, 2) + pow(y - sy, 2))

def calc_trajectory(area: list, vi=(9, 0)):
    x, y = (0, 0)
    (vx, vy) = vi

    cy = 0
    while calculate_distance((x, y)) < 1000:
        vx, vy, x, y = perform_step(vx, vy, x, y)
        cy = max(cy, y)
        if (x, y) in area:
            return vi, cy


def find_conditions(data: str, ymax= 45):
    import itertools
    (xrange, yrange) = parse_input(data)
    area = build_target_area(xrange, yrange)


    items = []
    for c in itertools.product(range(1, max(xrange)+1), range(min(yrange), abs(min(yrange)) + 1)):
        res = calc_trajectory(area, vi = c)
        if res:
            vi, mxy = res
            items.append(res)
    for vi, mxy in sorted(items, key=lambda c: c[1]):
        print(vi, mxy)
    print(len(items))
