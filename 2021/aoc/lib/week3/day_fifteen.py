import sys
from collections import defaultdict
from enum import Enum
from os import linesep

from aoc.lib.funcs import Priority
from aoc.lib.decos import log
from rich.console import Console


def in_grid(grid: list, i:int, j:int):
    if i < 0 or j < 0:
        return False
    if j >= len(grid):
        return False
    if i >= len(grid[j]):
        return False
    return True

def get_neighbors(grid: list, i:int, j: int):
    neighbors = [
        (x + i, y+j)
        for x in range(-1, 2)
        for y in range(-1, 2)
        if x != y and (x == 0 or y == 0)
    ]
    return [(x, y, grid[y][x]) for x, y in neighbors if in_grid(grid, x, y) ]

def get_location_value(grid: list, i: int, j: int):
    if i < 0 or i >= len(grid):
        return 9
    if j < 0 or j >= len(grid[i]):
        return 9

    return grid[j][i]

def parse_grid(input: str):
    lines = [x.strip() for x in input.split(linesep)]
    grid = []
    for i, row in enumerate(lines):
        vals = [int(x) for x in row if len(x) > 0]
        grid.append(vals)
    return grid

def get_goal(grid: list):
    x,y = (0,0)
    for j, row in enumerate(grid):
        for i, val in enumerate(row):
            x = max(i, x)
            y = max(j, y)
    return (x, y)

def length(grid: list, start: tuple, end: tuple):
    def inner(a: tuple, b: tuple):
        (x1, y1) = a
        (x2, y2) = b

        if a == b:
            return grid[y1][x1]

        nxt = (0,0)
        if x1 == x2:
            nxt = (x1, y1 + 1)
        else:
            nxt = (x1 + 1, y1)

        return grid[y1][x1] + inner(nxt, b)

    return inner(start, end)


def search(input: str):
    grid = parse_grid(input)
    start = (0, 0)
    goal = get_goal(grid)
    camefrom = {}
    searched = []
    def factory():
        return sys.maxsize
    def retrace_steps(val: tuple):
        total = [ val ]
        current = val
        while current in camefrom.keys():
            current = camefrom[current]
            if current != start:
                total.append(current)
        return total

    openNodes = [start]
    gScore = defaultdict(factory)
    gScore[start] = 0

    fScore = defaultdict(factory)
    fScore[start] = factory()

    while openNodes:
        current = min(openNodes, key=lambda c: fScore[c])
        if current not in searched:
            searched.append(current)

        if current == goal:
            return grid, retrace_steps(neighbor)
        if current in openNodes:
            openNodes.remove(current)

        (cx, cy) = current
        for x, y, _ in get_neighbors(grid, cx, cy):
            neighbor = (x, y)
            if neighbor == goal:
                camefrom[goal] = current
                return grid, retrace_steps(neighbor)

            tscore = gScore[current] + grid[cy][cx]
            if tscore < gScore[neighbor]:
                camefrom[neighbor] = current
                gScore[neighbor] = tscore
                fScore[neighbor] = tscore +  grid[y][x]
                if neighbor not in openNodes:
                    openNodes.append(neighbor)

def risk_assessment(input: str):
    grid, rest = search(input)
    rest.reverse()
    vals = [grid[y][x] for x,y in rest]
    result = sum(vals)
    return result

def djikstras_spa(input: str):
    grid = parse_grid(input)

    dist = {}
    prev = {}
    node_set = set()

    def inner():
        def mindist():
            vals = [
                (x, dist[x])
                for x in node_set
                ]
            ordered = sorted(vals, key= lambda c: c[1])
            (pt, dis) = ordered[0]
            return pt

        def neighbors(x:int, y:int):
            return [
                (r, s)
                for r, s, _ in get_neighbors(grid, x, y)
                if (r, s) in node_set
                ]

        gx,gy = (0,0)
        for j, row in enumerate(grid):
            for i, val in enumerate(row):
                gx = max(i, gx)
                gy = max(j, gy)
                dist[i, j] = sys.maxsize
                prev[i, j] = None
                node_set.add((i,j))
        goal = (gx, gy)
        dist[0,0] = 0
        while node_set:
            minim = mindist()
            node_set.remove(minim)

            if minim == goal:
                return goal

            (x, y) = minim
            assholes = neighbors(x, y)
            for n in assholes:
                (nx, ny) = n
                alt = dist[minim] + grid[ny][nx]
                if alt < dist[n]:
                    dist[n] = alt
                    prev[n] = minim

    goal = inner()

    path = []
    start = (0,0)
    while start != goal:
        path.append(goal)
        goal = prev[goal]
    path.reverse()
    return grid, path

def djikstras_priority(input: str):
    grid = parse_grid(input)
    dist = {}
    prev = {}
    queue = Priority()

    def inner():
        def neighbors(x:int, y:int):
            return [
                (r, s)
                for r, s, _ in get_neighbors(grid, x, y)
                if queue.task_exists((r, s))
                ]

        gx,gy = (0,0)
        dist[0,0] = 0
        for j, row in enumerate(grid):
            for i, val in enumerate(row):
                gx = max(i, gx)
                gy = max(j, gy)
                if (i, j) != (0, 0):
                    dist[i, j] = sys.maxsize
                    prev[i, j] = None
                queue.add_task((i, j), dist[i, j])

        goal = (gx, gy)
        running = True
        while running:
            try:
                minim = queue.pop_task()
            except KeyError:
                running = False
                continue

            if minim == goal:
                return goal

            (x, y) = minim
            assholes = neighbors(x, y)
            for n in assholes:
                (nx, ny) = n
                alt = dist[minim] + grid[ny][nx]
                if alt < dist[n]:
                    dist[n] = alt
                    prev[n] = minim
                    queue.add_task(n, alt)

    goal = inner()

    path = []
    start = (0,0)
    while start != goal:
        path.append(goal)
        goal = prev[goal]
    path.reverse()
    return grid, path

def djikstra_risk(input: str):
    grid, path = djikstras_spa(input)

    val = 0
    for x, y in path:
        val += grid[y][x]
    return val

class AlgoType(Enum):
    AStar = 0
    DjikstraSpa = 1
    DjikstraPri = 2

def rich_grid(data: str, output=AlgoType.AStar):
    cons = Console()
    if output == AlgoType.AStar:
        grid, path = search(data)
    elif output == AlgoType.DjikstraSpa:
        grid, path = djikstras_spa(data)
    elif output == AlgoType.DjikstraPri:
        grid, path = djikstras_priority(data)

    res = 0
    for j, row in enumerate(grid):
        rowsm = 0
        for i, val in enumerate(row):
            if (i,j) in path:
                rowsm += grid[j][i]
                cons.print(f"[red]{val}", end="")
            else:
                cons.print(f"[green]{val}", end="")
        cons.print("[blue]" + ((5 - len(str(res))) * "-"), end="")
        abc = str(rowsm).ljust(2, " ")
        cons.print(f"[purple]{res} + {abc}")
        res += rowsm

    cons.print(f"[purple]Total: {res}", justify="center")
    return grid, path

import time
def run_algos(data: str):
    for c in AlgoType:
        path = None
        t = time.time()
        if c == AlgoType.AStar:
            _, path = search(data)
        elif c == AlgoType.DjikstraSpa:
            _, path = djikstras_spa(data)
        elif c == AlgoType.DjikstraPri:
            _, path = djikstras_priority(data)
        diff = time.time() - t
        yield (c, path, diff)

def algorith_compare(size: int):
    import random
    grid = []
    for y in range(size):
        grid.append([])
        for x in range(size):
            grid[y].append(str(random.randint(1,9)))

    maze = linesep.join(["".join(x) for x in grid])

    def grid_sum(grid: list, path: list[tuple]):
        val = 0
        for x, y in path:
            val += int(grid[y][x])
        return val

    cols = ["red", "green", "blue", "magenta", "cyan"]
    paths = {}

    stats = []
    for algo, path, time in run_algos(maze):
        col = cols
        risk = grid_sum(grid, path)
        stats.append(f"{algo} Path: {len(path)} Risk: {risk} Time: {time:.4f}")
        paths[algo] = (path, risk, cols.pop(0))

    path, risk, col = paths.pop(AlgoType.AStar)

    osets = [(k, set(p).difference(path), c) for k, (p, r, c) in paths.items()]

    cons = Console()
    for y, row in enumerate(grid):
        for x, val in enumerate(row):
            marked = False
            if (x, y) in path:
                cons.print(f"[{col}]{val}", end="")
            else:
                for _, diff, cl in osets:
                    if (x,y) in diff and not marked:
                        cons.print(f"[{cl}]{val}", end="")
                        marked = True
                if not marked:
                    cons.print(f"[bright_black]{val}", end="")
        cons.print()

    for s in stats:
        print(s)

