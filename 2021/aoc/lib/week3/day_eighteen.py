from io import StringIO
import re

class Pair:
    def __init__(self):
        self.__left = None
        self.__right = None
    @property
    def left(self):
        return self.__left
    @property
    def right(self):
        return self.__right
    @left.setter
    def left(self, value):
        self.__left = value
    @right.setter
    def right(self, value):
        self.__right = value


def count(data: str, itm: str):
    return sum([1 if x == itm else 0 for x in list(data)])

def stream(data: StringIO, parent=Pair()):
    v = data.read(1)
    if v == "[":
        parent.left = stream(data, )
    if v == ",":
        pass
    if v == "]":
        pass
    if v.isdigit():
        pass

def process_snail(data: str):
    with StringIO(data) as fl:
        return stream(fl)


blocks = [ "[1,2]",
           "[[1,2],3]",
          "[9,[8,7]]",
          "[[1,9],[8,5]]",
          "[[[[1,2],[3,4]],[[5,6],[7,8]]],9]",
          "[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]",
          "[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]]"
          ]
for data in blocks:
    a = process_snail(data)
    print(a)

def splitit(data: str):
    ndx = 0
    for i in range(int(len(data) / 2)):
        l = data[i]
        r = data[len(data) -  (i + 1)]
        if l == ",":
            ndx = i
            break
        elif r == ",":
            ndx = len(data) - (1 + i)
            break
    return data[:ndx], data[ndx + 1:]

def getcontents(data: str):
    stk = []
    a = data[0]
    rest = data[1:]
    stk.append(a)
    block = []
    for i, c in enumerate(rest):
        if c == "[":
            stk.append(c)
        elif c == "]":
            stk.pop()
        if len(stk) == 0:
            break
        block.append(c)
    return "".join(block)

def process(data="[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"):
    PATTERN = r"\d+"
    cont = getcontents(data)
    l, r = splitit(cont)

    p = Pair()
    if re.match(PATTERN, l):
        p.left(int(l))
    else:
        p.left = process(l)

    if re.match(PATTERN, r):
        p.right = int(r)
    else:
        p.right = process(r)
    return p
