from os import linesep

def merge_row(blocks: list):
    rows = []
    splits = [x.split(linesep) for x in blocks]
    for i in range(len(splits[0])):
        l = [x[i] for x in splits]
        rows.append("".join(l))
    return linesep.join(rows)

def expand_block(data: str):
    def bump(val: int):
        if val == 9:
            return 1
        return val + 1
    lines = [x.strip() for x in data.split(linesep)]
    grid = []
    for j, row in enumerate(lines):
        vals = [int(x) for x in list(row)]
        bummped = [bump(x) for x in vals]
        grid.append("".join([str(y) for y in bummped]))
    return linesep.join(grid)

def expand_section(data: str):
    blocks = [data]
    for i in range(1, 5):
        last = blocks[-1]
        next = expand_block(last)
        blocks.append(next)

    row = merge_row(blocks)
    rows = [row]
    for i in range(1, 5):
        last = rows[-1]
        next = expand_block(last)
        rows.append(next)

    total = linesep.join(rows)
    return total
    