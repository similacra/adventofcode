from io import StringIO
from os import linesep

def parse(input: str):
    with StringIO(input) as fl:
        intp = fl.readline().strip()
        fl.readline()
        data = fl.read().strip()

    grid = {}

    for y, row in enumerate([x.strip() for x in data.split(linesep)]):
        for x, val in enumerate(row):
            if val == "#":
                grid[x, y] = "#"
    return grid, intp

def get_neighbors(grid: dict, x: int, y:int):
    lcls = [
        (x + j, y + i)
        for i in range(-1, 2)
        for j in  range(-1, 2)
    ]
    return [(x, y, grid[x,y]) if (x, y) in grid.keys() else (x, y, ".") for x, y in lcls ]

def perform_step(grid: dict, intp: list):
    def process_cell(x, y):
        lcls = get_neighbors(grid, x, y)
        bits = list(map(lambda c: "1" if c[2] == "#" else "0", lcls))
        val = int("".join(bits), base=2)
        return intp[val]

    nxt = {}
    for (x, y), v in grid.items():
        for r, s, _ in get_neighbors(grid, x, y):
            val = process_cell(r, s)
            if val == "#":
                nxt[r, s] = "#"

    for k, v in nxt.items():
        if v == ".":
            nxt.pop(k)

    return nxt

def print_grid(grid: dict):
    xs = [x for x, _ in grid.keys()]
    ys = [y for _, y in grid.keys()]

    for j in range(min(ys), max(ys) + 1):
        for i in range(min(xs), max(xs) + 1):
            if (i,j) in grid.keys():
                print("#", end="")
            else:
                print(".", end="")
        print()

def run_calculations(input: str, steps = 2):
    grid, intp = parse(input)

    nxt = grid
    for _ in range(steps):
        nxt = perform_step(nxt, intp)

    return len(nxt)

