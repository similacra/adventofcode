from io import StringIO
from enum import Enum

class OpType(Enum):
    Sum = 0
    Product = 1
    Min = 2
    Max = 3
    Greater = 5
    Less = 6
    Equal = 7


class Item:
    def __init__(self, version, packet):
            self.__version = version
            self.__packet = packet
    @property
    def version(self):
        return self.__version
    @property
    def packet(self):
        return self.__packet

class Literal(Item):
    def __init__(self, version, packet, value):
        Item.__init__(self, version, packet)
        self.__value = value
    @property
    def value(self):
        return self.__value

class Operator(Item):
    def __init__(self, version, packet):
        Item.__init__(self, version, packet)
        self.__children = []
        self.__operation = OpType(packet)
    @property
    def children(self):
        return self.__children
    @property
    def operation(self):
        return self.__operation
    def evaluate(self):
        vals = []
        for item in self.children:
            if isinstance(item, Operator):
                vals.append(item.evaluate())
            elif isinstance(item, Literal):
                vals.append(item.value)

        if self.operation == OpType.Sum:
            return sum(vals)
        elif self.operation == OpType.Product:
            res = 1
            for val in vals:
                res *= val
            return res
        elif self.operation == OpType.Min:
            return min(vals)
        elif self.operation == OpType.Max:
            return max(vals)
        elif self.operation == OpType.Greater:
            a, b, *_ = vals
            return 1 if a > b else 0
        elif self.operation == OpType.Less:
            a, b, *_ = vals
            return 1 if a < b else 0
        elif self.operation == OpType.Equal:
            a, b, *_ = vals
            return 1 if a == b else 0


def decode_data(data: str):
    vals = [int(x, base=16) for x in data]
    bstrs = [str(bin(x)) for x in vals]
    val = []
    for bst in bstrs:
        p = bst[2:].rjust(4, '0')
        val.append(p)
    return "".join(val)

def parseheader(data: StringIO):
    version = int(data.read(3), base=2)
    packet = int(data.read(3), base=2)
    return version, packet

def parseitem(data: StringIO):
    version, packet = parseheader(data)
    if packet == 4:
        return parseliteral(data, version, packet)
    else:
        return parseoperator(data, version, packet)

def parseliteral(data: StringIO, version: int, packet: int):
    packs = []
    run = True
    while run:
        head = data.read(1)
        run = (head == '1')
        pack = data.read(4)
        packs.append(pack)
    value = int("".join(packs), base=2)

    return Literal(version, packet, value)

def parseoperator(data: StringIO, version: int, packet: int):
    typeid = data.read(1)
    output = Operator(version, packet)
    if typeid == "0":
        bitleng = int(data.read(15), base=2)
        curr = data.tell()
        while data.tell() < curr + bitleng:
            output.children.append(parseitem(data))
    elif typeid == "1":
        bitleng = int(data.read(11), base=2)
        for _ in range(bitleng):
            output.children.append(parseitem(data))
    return output

def process(data: str):
    with StringIO(decode_data(data)) as fl:
        return parseitem(fl)

def versionsums(item):
    val = 0
    if isinstance(item, Operator):
        val += item.version
        for child in item.children:
            val += versionsums(child)
    elif isinstance(item, Literal):
        val += item.version
    return val

def printdata(item, header=""):
    val = 0
    if isinstance(item, Operator):
        print(f"{header}Operator Version: {item.version} Operation: {item.operation}")
        val += item.version
        for child in item.children:
            val += printdata(child, header + "--")
    elif isinstance(item, Literal):
        print(f"{header}Literal Version: {item.version} Packet: {item.packet} Value: {item.value}")
        val += item.version
    return val
