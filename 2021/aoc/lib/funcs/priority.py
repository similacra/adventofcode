import itertools
from dataclasses import dataclass, field
from typing import Any
from heapq import heappush, heappop

@dataclass(order=True)
class PrioritizedItem:
    priority: int
    item: Any=field(compare=False)

class Priority:
    REMOVED = '<removed-task>'      # placeholder for a removed task
    def __init__(self):
        self.__pq = []                         # list of entries arranged in a heap
        self.__entry_finder = {}               # mapping of tasks to entries
        self.__counter = itertools.count()     # unique sequence count

    @property
    def pq(self):
        return self.__pq
    @property
    def entry_finder(self):
        return self.__entry_finder
    @property
    def counter(self):
        return self.__counter

    def add_task(self, task, priority=0):
        'Add a new task or update the priority of an existing task'
        if task in self.entry_finder:
            self.remove_task(task)
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heappush(self.pq, entry)

    def remove_task(self, task):
        'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED

    def task_exists(self, task):
        return task in self.entry_finder.keys()

    def pop_task(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                return task
        raise KeyError('pop from an empty priority queue')