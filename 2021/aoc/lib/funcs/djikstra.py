import sys

from aoc.lib.dto.graph import Graph
from aoc.lib.funcs.priority import Priority


def djikstra_path(graph: Graph, start, goal):
    dist = {}
    prev = {}
    queue = Priority()

    for node in graph.nodes.keys():
        dist[node] = 0 if node == start else sys.maxsize
        queue.add_task(node, dist[node])

    prev[start] = None

    running = True
    while running:
        try:
            minim = queue.pop_task()
        except KeyError:
            running = False
            continue
        if minim == goal:
            running = False
            continue

        (x, y) = minim
        assholes = graph.nodes[x, y]
        for n in assholes:
            alt = dist[minim] + graph.edges[(x,y), n]
            if alt < dist[n]:
                dist[n] = alt
                prev[n] = minim
                queue.add_task(n, alt)
    path = []
    while start != goal:
        path.append(goal)
        goal = prev[goal]
    path.reverse()

    return path