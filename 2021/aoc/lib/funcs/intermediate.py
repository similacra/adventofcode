def collinear(x1, y1, x2, y2, x3, y3):

    """Calculation the area of
    triangle. We have skipped
    multiplication with 0.5 to
    avoid floating point computations"""
    a = x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)

    return a == 0


def compute_intermediates(start: tuple, end: tuple):
    x1, y1 = start
    x2, y2 = end
    xmin, xmax = sorted([x1, x2])
    ymin, ymax = sorted([y1, y2])

    if xmin == xmax:
        for y in range(int(ymin + 1), int(ymax)):
            yield (xmin, float(y))
    elif ymin == ymax:
        for x in range(int(xmin + 1), int(xmax)):
            yield (float(x), ymin)
    else:
        for x in range(int(xmin + 1), int(xmax)):
            for y in range(int(ymin + 1), int(ymax)):
                if collinear(x1, y1, x2, y2, x, y):
                    yield (float(x), float(y))
