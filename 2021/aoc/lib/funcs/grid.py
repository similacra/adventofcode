def buildgrid(width = 10, height = 10):
    grid = []
    for j in range(height):
        grid.append([])
        for i in range(width):
            grid[j].append(0)
    return grid
