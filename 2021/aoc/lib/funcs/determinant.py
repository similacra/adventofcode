def determinant(grid: list):
    if len(grid) == 1:
        if len(grid[0]) == 1:
            return grid[0][0]

    def exclude(i, j):
        excl = []
        inc = 0
        for y, row in enumerate(grid):
            if y == i:
                continue
            excl.append([])
            for x, val in enumerate(row):
                if x == j:
                    continue
                excl[inc].append(val)
            inc += 1

        return excl

    accum = 0
    for j, item in enumerate(grid[0]):
        others = exclude(0, j)

        if (j % 2) != 0:
            item *= -1
        accum += item * determinant(others)
    return accum

def collinear_mat(x1, y1, x2, y2, x3, y3):
    mat = [
        [x1, x2, x3],
        [y1, y2, y3],
        [1, 1, 1]
    ]

    return determinant(mat) == 0

def mag(vect: tuple):
    from math import sqrt

    v = sum(map(lambda c: c * c, vect))
    return sqrt(v)


def rotate_x(theta: float):
    from math import sin, cos

    return [[1, 0, 0], [0, cos(theta), -1 * sin(theta)], [0, sin(theta), cos(theta)]]


def rotate_y(theta: float):
    from math import sin, cos

    return [[cos(theta), 0, sin(theta)], [0, 1, 0], [-sin(theta), 0, cos(theta)]]


def rotate_z(theta: float):
    from math import sin, cos

    return [[cos(theta), -sin(theta), 0], [sin(theta), cos(theta), 0], [0, 0, 1]]


def mult(items: list, acc=1):
    if len(items) == 0:
        return acc
    return mult(items[1:], items[0] * acc)


def dotproduct(vec: tuple, nxt: tuple):
    return sum([mult(x) for x in zip(vec, nxt)])


def anglebetween(vec: tuple, nxt: tuple):
    from math import acos

    dot = dotproduct(vec, nxt)
    magv = mag(vec)
    magn = mag(nxt)
    return acos(dot / (magv * magn))


def matmult(mat: list, nxt: list):
    if len(nxt) != len(mat[0]):
        return None

    def col(oth: list, ndx: int):
        return [x[ndx] for x in oth]

    result = []
    for y, row in enumerate(mat):
        result.append([])
        for x, _ in enumerate(row):
            if (x + 1) > len(nxt[x]):
                continue
            c = col(nxt, x)
            d = dotproduct(row, c)
            result[y].append(d)
    return result
