class Grid:
    def __init__(self, dimensions=10):
        self.__grid = []
        for i in range(dimensions):
            self.__grid.append([0 for _ in range(dimensions)])

    def __str__(self):
        def mapper(val):
            if val == 0:
                return "."
            return str(val)

        vals = []
        for i in self.grid:
            vals.append("".join(list(map(mapper, i))))
        return "\n".join(vals)

    @property
    def grid(self):
        return self.__grid

    def evaluate(self, sentinel: int):
        val = 0
        for i in self.grid:
            for j in i:
                if j >= sentinel:
                    val += 1
        return val
