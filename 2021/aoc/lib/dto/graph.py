from aoc.lib.week3.day_fifteen import get_neighbors

class Graph:
    def __init__(self):
        self.__nodes = {}
        self.__edges = {}
    @property
    def nodes(self):
        return self.__nodes
    @property
    def edges(self):
        return self.__edges

def parse_graph(grid: list):
    g = Graph()
    for j, row in enumerate(grid):
        for i, val in enumerate(row):
            g.nodes[i,j] = []
            for x, y, nd in get_neighbors(grid, i, j):
                g.edges[(i, j), (x, y)] = int(nd)
                g.nodes[i,j].append((x, y))
    return g
