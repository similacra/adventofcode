from aoc.lib.dto.direction import Direction
from aoc.lib.funcs import compute_intermediates


class Line:
    def __init__(self, start, end) -> None:
        self.__start = start
        self.__end = end
        self.__parts = list(compute_intermediates(start, end))

    @property
    def start(self):
        return self.__start

    @property
    def end(self):
        return self.__end

    @property
    def parts(self):
        return self.__parts

    @property
    def direction(self):
        x1, y1 = self.start
        x2, y2 = self.end
        if x1 == x2:
            return Direction.VERTICAL
        elif y1 == y2:
            return Direction.HORIZONTAL
        else:
            return Direction.DIAGONAL

    @property
    def magnitude(self):
        import math

        x1, y1 = self.start
        x2, y2 = self.end

        return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2))

    def __str__(self):
        return f"{self.start} {self.end} {self.magnitude}"
