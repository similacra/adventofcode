import os


class Board:
    def __init__(self):
        self.__markings = [
            [False, False, False, False, False],
            [False, False, False, False, False],
            [False, False, False, False, False],
            [False, False, False, False, False],
            [False, False, False, False, False],
        ]
        self.__rows = [
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
        ]

    @property
    def markings(self):
        return self.__markings

    @property
    def rows(self):
        return self.__rows

    @property
    def empty(self):
        for i in self.rows:
            for j in i:
                if j >= 0:
                    return False
        return True

    @property
    def unmarked(self):
        val = 0
        for i in range(len(self.markings)):
            for j in range(len(self.markings[i])):
                if not self.markings[i][j]:
                    val += self.rows[i][j]
        return val

    @property
    def bingo(self):
        for row in self.markings:
            if all(row):
                return True
        for i in range(len(self.markings)):
            cols = [x[i] for x in self.markings]
            if all(cols):
                return True
        return False

    def __str__(self):
        parts = []
        for i in range(len(self.rows)):
            for j in range(len(self.rows[i])):
                parts.append(f"{self.rows[i][j]}({self.markings[i][j]}) ")
            parts.append(os.linesep)
        return "".join(parts)

    def __eq__(self, other: object):
        if other is not Board:
            return False
        for i in range(len(self.rows)):
            for j in range(len(self.rows[i])):
                if self.rows[i][j] != other.rows[i][j]:
                    return False
        return True

    def check_value(self, val: int):
        for i in range(len(self.rows)):
            for j in range(len(self.rows[i])):
                if self.rows[i][j] == val:
                    self.markings[i][j] = True
                    return
