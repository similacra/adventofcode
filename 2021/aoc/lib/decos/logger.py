import time

def log(name="FunctionName"):
    def wrap(func):
        def run(*args, **kwargs):
            start = time.time()
            val = func(*args, **kwargs)
            dur = time.time() - start
            print(f"Function: {name} ran in {dur:.4f}")
            return val
        return run
    return wrap