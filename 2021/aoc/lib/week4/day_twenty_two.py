from os import linesep


def parse(input: str):
    instructions = []
    for line in [x.strip() for x in input.split(linesep) if len(x.strip()) > 0]:
        action, rng = line.split(" ")
        rngs = {}
        for i in rng.split(","):
            axis, nums = i.split("=")
            bottom, top = [int(x) for x in nums.split("..")]
            bottom = max(bottom, -50)
            top = min(top, 50)
            rngs[axis] = (bottom, top)
        instructions.append((action, rngs))
    return instructions


def process(input: str):
    instructions = parse(input)

    grid = {}
    for action, rngs in instructions:
        xmin, xmax = rngs['x']
        ymin, ymax = rngs['y']
        zmin, zmax = rngs['z']

        for z in range(zmin, zmax + 1):
            for y in range(ymin, ymax + 1):
                for x in range(xmin, xmax + 1):
                    if action == 'on':
                        grid[x, y, z ] = 1
                    elif (x, y, z) in grid:
                        grid.pop((x,y,z))

    return len(grid)
