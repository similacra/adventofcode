import pytest
from aoc.lib.week1.day_one import calc_day_1_1, calc_day_1_2, WindowedIncreaser


@pytest.fixture
def file_data():
    import sys

    sys.setrecursionlimit(1000000)

    lines = None
    with open("./tests/week1/inputs/day_001.txt") as fl:
        lines = fl.readlines()
    return list(map(int, lines))


@pytest.fixture
def input_data():
    return [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]


class TestFirstDay:
    def test_part_one(self, input_data):
        assert 7 == calc_day_1_1(input_data)

    def test_part_two(self, input_data):
        assert 5 == calc_day_1_2(input_data)

    def test_part_one_with_input(self, file_data):
        assert 1557 == calc_day_1_1(file_data)

    def test_part_two_with_input(self, file_data):
        assert 1608 == calc_day_1_2(file_data)

    def test_part_one_class(self, input_data):
        tool = WindowedIncreaser()
        assert 7 == tool(input_data)

    def test_part_two_class(self, input_data):
        tool = WindowedIncreaser(3)
        assert 5 == tool(input_data)

    def test_part_one_class_input(self, file_data):
        tool = WindowedIncreaser()
        assert 1557 == tool(file_data)

    def test_part_two_class_input(self, file_data):
        tool = WindowedIncreaser(3)
        assert 1608 == tool(file_data)
