import pytest
from aoc.lib.week1.day_three import (
    compute_gamma,
    compute_epsilom,
    compute_O2_rating,
    compute_CO2_rating,
)


@pytest.fixture
def test_data():
    return [
        "00100",
        "11110",
        "10110",
        "10111",
        "10101",
        "01111",
        "00111",
        "11100",
        "10000",
        "11001",
        "00010",
        "01010",
    ]


@pytest.fixture
def file_data():
    lines = None
    with open("./tests/week1/inputs/day_003.txt") as fl:
        lines = fl.readlines()
    return lines


class TestThirdDay:
    def test_compute_gamma(self, test_data):
        assert 22 == compute_gamma(test_data)

    def test_compute_epsilom(self, test_data):
        assert 9 == compute_epsilom(test_data)

    def test_compute_gamma_file(self, file_data):
        assert 1300 == compute_gamma(file_data)

    def test_compute_epsilom_file(self, file_data):
        assert 2795 == compute_epsilom(file_data)

    def test_compute_O2(self, test_data):
        assert 23 == compute_O2_rating(test_data)

    def test_compute_CO2(self, test_data):
        assert 10 == compute_CO2_rating(test_data)

    def test_compute_O2_file(self, file_data):
        assert 1327 == compute_O2_rating(file_data)

    def test_compute_CO2_file(self, file_data):
        assert 3429 == compute_CO2_rating(file_data)
