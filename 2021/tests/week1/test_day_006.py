import pytest
from aoc.lib.week1.day_six import calculate


@pytest.fixture
def test_data():
    return """3,4,3,1,2"""


@pytest.fixture
def file_data():
    with open("./tests/week1/inputs/day_006.txt") as fl:
        return fl.read()


class TestSixthDay:
    def test_fish_count_80_days(self, test_data):
        assert 5934 == calculate(test_data, 80)

    def test_fish_count_80_days_file(self, file_data):
        assert 343441 == calculate(file_data, 80)

    def test_fish_count_256_days(self, test_data):
        assert 26984457539 == calculate(test_data, 256)

    def test_fish_count_256_days_file(self, file_data):
        assert 1569108373832 == calculate(file_data, 256)
