import pytest, os
from aoc.lib.week1.day_four import solve


@pytest.fixture
def test_data():
    return """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
8  2 23  4 24
21  9 14 16  7
6 10  3 18  5
1 12 20 15 19

3 15  0  2 22
9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
2  0 12  3  7"""


@pytest.fixture
def file_data():
    with open("./tests/week1/inputs/day_004.txt") as fl:
        return fl.read()


class TestFourthDay:
    def test_when_bingo_happens(self, test_data):
        assert 4512 == solve(test_data)

    def test_when_bingo_happens_file(self, file_data):
        assert 58374 == solve(file_data)

    def test_lose(self, test_data):
        assert 1924 == solve(test_data, True)

    def test_lose_file(self, file_data):
        assert 11377 == solve(file_data, True)
