import pytest

from aoc.lib.week1.day_five import build_grid


@pytest.fixture
def test_data():
    return """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"""


@pytest.fixture
def file_data():
    with open("./tests/week1/inputs/day_005.txt") as fl:
        return fl.read()


class TestFifthDay:
    def test_overlap(self, test_data):
        assert 12 == build_grid(test_data)

    def test_overlap_file(self, file_data):
        assert 20299 == build_grid(file_data)

    def test_overlap_exclude_diagonals(self, test_data):
        assert 5 == build_grid(test_data, False)

    def test_overlap_exclude_diagonals_file(self, file_data):
        assert 5608 == build_grid(file_data, False)
