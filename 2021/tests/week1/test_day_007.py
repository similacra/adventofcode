import pytest
from aoc.lib.week1.day_seven import calculate_fuel_cost


def test_data():
    return "16,1,2,0,4,2,7,1,2,14"


def file_data():
    with open("./tests/week1/inputs/day_007.txt") as fl:
        return fl.read()


input_data = [
    (test_data(), False, 37),
    (test_data(), True, 168),
    (file_data(), False, 353800),
    (file_data(), True, 98119739),
]


@pytest.mark.parametrize("input,accumulate,expected", input_data)
def test_calculate_fuel_cost(input, accumulate, expected):
    assert expected == calculate_fuel_cost(input, accumulate)
