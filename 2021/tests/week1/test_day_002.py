import pytest
from aoc.lib.week1.day_two import Move, Direction, perform_moves, perform_moves_with_aim


def map_item(value: str):
    direc, val, *_ = value.split(" ")

    if direc == "up":
        return Move(Direction.Up, int(val))
    elif direc == "down":
        return Move(Direction.Down, int(val))
    elif direc == "forward":
        return Move(Direction.Forward, int(val))
    raise ValueError(f"Cannot parse line {value}")


@pytest.fixture
def file_data():
    import sys

    sys.setrecursionlimit(1000000)

    lines = None
    with open("./tests/week1/inputs/day_002.txt") as fl:
        lines = fl.readlines()
    return list(map(map_item, lines))


@pytest.fixture
def moves():
    return [
        Move(Direction.Forward, 5),
        Move(Direction.Down, 5),
        Move(Direction.Forward, 8),
        Move(Direction.Up, 3),
        Move(Direction.Down, 8),
        Move(Direction.Forward, 2),
    ]


class TestSecondDay:
    def test_movement(self, moves):
        x, y = perform_moves(moves)
        assert x == 15
        assert y == 10

    def test_movement_file(self, file_data):
        x, y = perform_moves(file_data)
        assert x == 1850
        assert y == 927

    def test_movement_with_aim(self, moves):
        x, y = perform_moves_with_aim(moves)
        assert x == 15
        assert y == 60

    def test_movement_with_aim_file(self, file_data):
        x, y = perform_moves_with_aim(file_data)
        assert x == 1850
        assert y == 692961
