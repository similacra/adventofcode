import pytest
from aoc.lib.week2.day_nine import calculate_risk_level, find_basins


@pytest.fixture
def test_data():
    return """2199943210
3987894921
9856789892
8767896789
9899965678"""


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_009.txt") as fl:
        return fl.read()


class TestNinthDay:
    def test_sink_detection(self, test_data):
        assert 15 == calculate_risk_level(test_data)

    def test_sink_detection_file(self, file_data):
        assert 500 == calculate_risk_level(file_data)

    def test_find_basins(self, test_data):
        assert 1134 == find_basins(test_data)

    def test_find_basins_file(self, file_data):
        assert 970200 == find_basins(file_data)
