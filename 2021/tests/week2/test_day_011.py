import pytest
from aoc.lib.week2.day_eleven import calculate_flashing_octopus, calculate_synced_flash


@pytest.fixture
def test_data():
    return """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"""


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_011.txt") as fl:
        return fl.read()


class TestEleventhDay:
    def test_flashing_octopus(self, test_data):
        assert 1656 == calculate_flashing_octopus(test_data, 100)

    def test_flashing_octopus_file(self, file_data):
        assert 1632 == calculate_flashing_octopus(file_data, 100)

    def test_flashing_octopus_sync(self, test_data):
        assert 195 == calculate_synced_flash(test_data)

    def test_flashing_octopus_sync_file(self, file_data):
        assert 303 == calculate_synced_flash(file_data)
