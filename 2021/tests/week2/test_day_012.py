import pytest
from aoc.lib.week2.day_twelve import compute_paths, better_compute_paths

@pytest.fixture
def test_data():
    return """start-A
start-b
A-c
A-b
b-d
A-end
b-end"""


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_012.txt") as fl:
        return fl.read()

class TestTwelvethDay:
    def test_path_finder(self, test_data):
        assert 10 == compute_paths(test_data)
    def test_path_finder_file(self, file_data):
        assert 4659 == compute_paths(file_data)
    def test_path_finder_small(self, test_data):
        assert 36 == better_compute_paths(test_data, True)
    def test_path_finder_small_file(self, file_data):
        assert 148962 == better_compute_paths(file_data, True)
