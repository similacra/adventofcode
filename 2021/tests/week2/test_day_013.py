import pytest
import aoc.lib.week2.day_thirteen as cd

@pytest.fixture
def test_data():
    return """6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"""


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_013.txt") as fl:
        return fl.read()

def grid_str(grid: list):
    from os import linesep
    result =[]
    for i, row in enumerate(grid):
        result.append("".join(map(lambda c: "." if c == 0 else "#", row)))
    return linesep.join(result)

class TestThirteenthDay:
    def test_dot_count_after_one_fold(self, test_data):
        assert 17 == cd.calculate_dots_after_folds(test_data, 1)
    def test_dot_count_after_one_fold_file(self, file_data):
        assert 765 == cd.calculate_dots_after_folds(file_data, 1)
    
    def test_dot_rework_after_one_fold(self, test_data):
        last = cd.process_folds(test_data, 1)
        assert 17 == cd.one_count(last)
    def test_dot_rework_after_one_fold_file(self, file_data):
        last = cd.process_folds(file_data, 1)
        assert 765 == cd.one_count(last)
    
    def test_dot_rework_final(self, file_data):
        from os import linesep
        expected = linesep.join([ 
                "###..####.#..#.####.#....###...##..#..#.",
                "#..#....#.#.#.....#.#....#..#.#..#.#..#.",
                "#..#...#..##.....#..#....#..#.#....####.",
                "###...#...#.#...#...#....###..#.##.#..#.",
                "#.#..#....#.#..#....#....#....#..#.#..#.",
                "#..#.####.#..#.####.####.#.....###.#..#."])
        last = grid_str(cd.process_folds(file_data))
        assert expected == last
            
