import pytest
import aoc.lib.week2.day_fourteen as cd

@pytest.fixture
def test_data():
    return """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"""


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_014.txt") as fl:
        return fl.read()


class TestFourteenthDay:
    def test_polymerization(self, test_data):
        assert 1588 == cd.compute_polymerization(test_data)
    def test_polymerization_file(self, file_data):
        assert 2851 == cd.compute_polymerization(file_data)
    def test_polymerization_better(self, test_data):
        assert 1588 == cd.compute_better_polymerization(test_data)
    def test_polymerization_better_file(self, file_data):
        assert 2851 == cd.compute_better_polymerization(file_data)
    def test_polymerization_better_file_big(self, file_data):
        assert 10002813279337 == cd.compute_better_polymerization(file_data, 40)
