import pytest
from aoc.lib.week2.day_ten import find_corrupted_lines, repair_corrupted_lines


@pytest.fixture
def test_data():
    return """[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
    """


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_010.txt") as fl:
        return fl.read()


class TestTenthDay:
    def test_syntax_parsing(self, test_data):
        assert 26397 == find_corrupted_lines(test_data)

    def test_syntax_parsing_file(self, file_data):
        assert 341823 == find_corrupted_lines(file_data)

    def test_corrupted_repair(self, test_data):
        assert 288957 == repair_corrupted_lines(test_data)

    def test_corrupted_repair_file(self, file_data):
        assert 2801302861 == repair_corrupted_lines(file_data)
