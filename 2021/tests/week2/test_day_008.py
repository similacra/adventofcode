import pytest
from aoc.lib.week2.day_eight import calculate_signal_wires, calculate_signal_result


@pytest.fixture
def test_data():
    return """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
    edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
    fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
    fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
    aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
    fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
    dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
    bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
    egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
    gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"""


@pytest.fixture
def test_data_part_2():
    return "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"


@pytest.fixture
def file_data():
    with open("./tests/week2/inputs/day_008.txt") as fl:
        return fl.read()


class TestEighthDay:
    def test_signal_parser(self, test_data):
        assert 26 == calculate_signal_wires(test_data)

    def test_signal_parser_file(self, file_data):
        assert 449 == calculate_signal_wires(file_data)

    def test_signal_result(self, test_data_part_2):
        assert 5353 == calculate_signal_result(test_data_part_2)

    def test_signal_result_file(self, file_data):
        assert 968175 == calculate_signal_result(file_data)
