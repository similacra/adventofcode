import pytest
import aoc.lib.week3.day_twenty as cd

@pytest.fixture
def test_data():
    return """..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###"""

@pytest.fixture
def file_data():
    with open("./tests/week3/inputs/day_020.txt") as fl:
        return fl.read()

class TestTwentyethDay:
    def test_image_enhancement(self, test_data):
        assert 35 == cd.run_calculations(test_data)
    def test_image_enhancement_file(self, file_data):
        assert -1 == cd.run_calculations(file_data)