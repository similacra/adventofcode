import pytest
import aoc.lib.week3.day_seventeen as cd

@pytest.fixture
def test_data():
    return "target area: x=230..283, y=-107..-57"

@pytest.fixture
def file_data():
    with open("./tests/week3/inputs/day_017.txt") as fl:
        return fl.read()

class TestSeventeenthDay:
    def first_test(self, test_data):
        assert 12 == cd.calc_trajectory()