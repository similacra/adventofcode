import pytest
import aoc.lib.week3.day_fifteen as cd
from aoc.lib.week3.day_fifteen_too import expand_section

@pytest.fixture
def test_data():
    return """1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"""

@pytest.fixture
def file_data():
    with open("./tests/week3/inputs/day_015.txt") as fl:
        return fl.read()

class TestFifteenthDay:
    def test_efficient_path(self, test_data):
        assert 40 == cd.risk_assessment(test_data)
    def test_efficient_path_file(self, file_data):
        assert 398 == cd.risk_assessment(file_data)
    def test_a_star_djikstra_match(self, file_data):
        astar = cd.risk_assessment(file_data)
        dji = cd.djikstra_risk(file_data)
        assert astar == dji
    def test_djikstra_priority_expanded_file(self, file_data):
        grid = expand_section(file_data)

        grid, path = cd.djikstras_priority(grid)

        val = 0
        for p in path:
            (x, y) = p
            val += grid[y][x]
        assert 2817 == val

