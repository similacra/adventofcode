from aoc.lib.week3.day_twenty_one import calculate


class TestTwentyFirstDay:
    def test_calc_loser_rollcount(self):
        assert 739785 == calculate()
    def test_calc_loser_rollcount_file(self):
        assert 989352 == calculate(5, 9)
        