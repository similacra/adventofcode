from os import linesep
from rich.console import Console
from aoc.lib.funcs.djikstra import djikstra_path
from aoc.lib.dto.graph import parse_graph

with open("/Users/sam/stuff/adventofcode/2021/tests/week3/inputs/day_015_1.txt") as fl:
    data = fl.read()

grid = [list(map(int, x)) for x in data.split(linesep)]

graph = parse_graph(grid)


cols = ["red", "green", "blue", "magenta", "cyan", "bright_yellow"]
def pammer(points):
    (start, goal) = points
    return (cols.pop(), [start, *djikstra_path(graph, start, goal)])

paths = [
    ((0,0), (99, 99)),
    ((0, 99), (99, 0)),
    ((0, 49), (99, 49)),
    ((49, 0), (49, 99)),
    ((0, 50), (99, 50)),
    ((50, 0), (50, 99))
]

paths = list(map(pammer, paths))


cons = Console()
for j, row in enumerate(grid):
    for i, val in enumerate(row):
        marked = False
        for col, path in paths:
            if marked:
                continue
            if (i, j) in path:
                marked = True
                cons.print(f"[{col}]{val}", end="")
        if not marked:
            cons.print(f"[bright_black]{val}", end="")
    cons.print()
